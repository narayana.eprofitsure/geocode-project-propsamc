from django.conf.urls import url
from django.conf import settings
from .import views
from django.conf.urls.static import static

urlpatterns = [
	url(r'^$', views.GetGeocode, name='geo'), # Notice the URL has been named
    url(r'^upload$', views.UploadExcel, name='geo_upload'),
]