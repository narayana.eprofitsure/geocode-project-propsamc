import os
import sys
import tempfile
import django

sys.path.append("..")

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "geocode.settings")
if __name__ == "__main__":
    django.setup()

import openpyxl
import requests
from io import BytesIO
from django.http import HttpResponse
import xlwt
api_key = 'H7smvxRGGQ53jJnFW2d2diSiP9ovu6tZ'


def read_excel(fil_input):
	wb = openpyxl.load_workbook(fil_input)
	worksheet = wb["GeoSheet"]
	excel_data = list()

	for row in worksheet.iter_rows():
	    row_data = list()
	    for cell in row:
	        row_data.append(str(cell.value))
	    excel_data.append(row_data)

	result = excel_data
	print(result)
	obj = 'success'
	return result


def get_geocode(address):
	geo = requests.get('http://open.mapquestapi.com/geocoding/v1/address?key=' + api_key + '&location=' + address)
	print(geo, 'geo output')
	data = geo.json()
	# print(data, 'dataa')
	final_output = {}
	final_output['lattitude'] = data['results'][0]['locations'][0]['latLng']['lat']
	final_output['longitutde'] = data['results'][0]['locations'][0]['latLng']['lng']
	return final_output


def main_func(file):
	# file = '/home/narayana/Documents/geocode.xlsx'
	final_output_list = list()
	excel_obj = read_excel(file)
	for i in excel_obj[1:]:
		if i[1]:
			geo_code_obj = get_geocode(i[1])
			i.append(geo_code_obj['lattitude'])
			i.append(geo_code_obj['longitutde'])
			final_output_list.append(i)
		print(geo_code_obj, 'finally')
	print(final_output_list, 'list of data')
	return final_output_list


import xlsxwriter
def write_to_excel(file):
	# eg = [['1', 'Malleshwaram, Bengaluru, Karnataka, India', 'India', 13.002735, 77.570325], ['2', 'Whitefield, Bengaluru, Karnataka, India', 'India', 12.969637, 77.749745]] 
	call_main = main_func(file)
	temp = BytesIO()
	workbook = xlsxwriter.Workbook(temp)
	worksheet = workbook.add_worksheet()
	bold = workbook.add_format({'bold': True})
	row = 0
	column = 0

	columns = [
	    "Sl no",
	    "Address",
	    "Country",
	    "Lattitude",
	    "Longitutde"
	]

	for item in columns: 
	    print(item)
	    worksheet.write(row, column, item, bold)
	    column += 1

	column = 0

	for item in call_main:
	    print(row, 'row')
	    
	    row += 1
	    for i in item:
	        worksheet.write(row, column, str(i))
	        column += 1
	    column = 0
	workbook.close()
	return temp


def write_to_excel_download(file):
	# eg = [['1', 'Malleshwaram, Bengaluru, Karnataka, India', 'India', 13.002735, 77.570325], ['2', 'Whitefield, Bengaluru, Karnataka, India', 'India', 12.969637, 77.749745]] 
	response = HttpResponse('You have successfully uploaded the excel and the output is mailed to your provided email id and its downloaded in local machine.', content_type="application/ms-excel")
	response[
	    "Content-Disposition"
	] = 'attachment; filename="geocode_address.xlsx"'

	call_main = main_func(file)

	wb = xlwt.Workbook(encoding="utf-8")
	ws = wb.add_sheet("GeoSheet")

	font_style = xlwt.XFStyle()
	font_style.font.bold = True

	row_num = 0

	columns = [
	    "Sl no",
	    "Address",
	    "Country",
	    "Lattitude",
	    "Longitutde"
	]

	for col_num in range(len(columns)):
	    ws.write(row_num, col_num, columns[col_num], font_style)

	address_data_list = []
	for dat in call_main:
	    # dat_tuple = dat.values()
	    dat_tuple = tuple(dat)
	    address_data_list.append(dat_tuple)

	for row in address_data_list:
	    row_num += 1
	    for col_num in range(len(row)):
	        ws.write(row_num, col_num, row[col_num])

	wb.save(response)
	return response


if __name__ == "__main__":
	file = '/home/narayana/Documents/geocode.xlsx'
	file_conver = write_to_excel()
	print(file_conver)